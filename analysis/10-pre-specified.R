# Metainfo ----------------------------------------------------------------

# @AUTHOR: Chun-Jie Liu
# @CONTACT: chunjie.sam.liu.at.gmail.com
# @DATE: Fri Jul 16 12:08:16 2021
# @DESCRIPTION: 10-pre-specified.R

# Library -----------------------------------------------------------------

library(magrittr)
library(ggplot2)
library(DESeq2)
library(mlr)

# src ---------------------------------------------------------------------

source(file = "src/utils.R")
source(file = "src/performance.R")
source(file = "src/plots.R")


# Load data ---------------------------------------------------------------

wuhan.se <- readr::read_rds(file = "data/rda/wuhan.se.rds.gz")
tom.se <- readr::read_rds(file = "data/rda/tom.se.rds.gz")

panel <- readr::read_rds(file = "data/rda/panel.rds.gz")
model.list <- readr::read_rds(file = "data/rda/model.list.rds.gz")

bm.hc.task <- readr::read_rds(file = "data/rda/bm.hc.task.rds.gz")
bm.bam.task <- readr::read_rds(file = "data/rda/bm.bam.task.rds.gz")

# Function ----------------------------------------------------------------
fn_performance_ensemble <- function(.x, .y) {
  .perf <- fn_performance(.x, .y)
  .metrics <- fn_get_metrics(.perf)
  .plot <- fn_get_auc_plot(.perf, .metrics)

  list(
    performance = .perf,
    metrics = .metrics,
    plot = .plot
  )
}

fn_thresh_vs_perf <- function(.pred, .gridsize = 100) {
  .gtvp <- generateThreshVsPerfData(.pred, measures = list(tpr, tnr), gridsize = .gridsize)
  .gtvp$data %>%
    tibble::as_tibble() %>%
    dplyr::mutate(d = purrr::map(.x = threshold, .f = function(.threshold) {
      .pts <- setThreshold(.pred, .threshold)

      .auc <- fn_auc_ci(.p = .pred)
      .rocm <- mlr::calculateROCMeasures(.pts)
      .kappa_f1 <- mlr::performance(pred = .pred, measures = list(mlr::kappa, mlr::f1))
      .rocm_epi <- epiR::epi.tests(dat = t(.rocm$confusion.matrix))

      tibble::tibble(
        auc_ci = list(unlist(c(.auc$auc[2], .auc$auc[1], .auc$auc[3]))),
        acc_ci = list(unlist(.rocm_epi$detail$diag.ac)),
        tpr_ci = list(unlist(.rocm_epi$detail$se)),
        tnr_ci = list(unlist(.rocm_epi$detail$sp)),
        ppv_ci = list(unlist(.rocm_epi$detail$pv.pos)),
        npv_ci = list(unlist(.rocm_epi$detail$pv.neg)),
        kappa = .kappa_f1["kappa"],
        f1 = .kappa_f1["f1"]
      ) %>%
        dplyr::mutate_if(
          .predicate = rlang::is_list,
          .funs = ~ purrr::map(.x = ., .f = function(.x) {
            glue::glue('{sprintf("%.3f", .x[1])} ({sprintf("%.3f", .x[2])} - {sprintf("%.3f", .x[3])})')
          })
        ) %>%
        dplyr::mutate_if(
          .predicate = rlang::is_double,
          .funs = ~ purrr::map(.x = ., .f = function(.x) {
            sprintf("%.3f", .x)
          })
        ) %>%
        dplyr::mutate_if(
          .predicate = rlang::is_list,
          .funs = unlist
        )
    })) %>%
    tidyr::unnest(cols = d) ->
    .gtvp_ci

  .gtvp_ci
}

fn_predict_pre_specified <- function(.task) {
  purrr::map(
    .x = .task,
    .f = function(.x) {
      purrr::map2(
        .x = model.list,
        .y = .x,
        .f = fn_performance_ensemble
      )
    }
  ) ->
    .performance

  .th_ci <- purrr::map(
    .x = .performance,
    .f = function(.t) {
      purrr::map(
        .x = .t,
        .f = function(.m) {
          purrr::map(
            .x = .m$performance,
            .f = function(.c) {
              fn_thresh_vs_perf(.c$pred)
            }
          )
        }
      )
    }
  )

  .th_ci %>%
    tibble::enframe(name = "hc_bam", value = "a") %>%
    dplyr::mutate(a = purrr::map(.x = a, .f = function(.a) {
      tibble::enframe(.a, name = "panel", value = "b") %>%
        dplyr::mutate(b = purrr::map(.x = b, .f = function(.b) {
          tibble::enframe(.b, name = "cohort", value = "m") %>%
            tidyr::unnest(cols = m)
        }))
    })) %>%
    tidyr::unnest(cols = a) %>%
    tidyr::unnest(cols = b)

}

# task --------------------------------------------------------------------


bm.hc.bam.task <- readr::read_rds(file = "data/rda/bm.hc.bam.task.rds.gz")
bm.pre.spe <- fn_predict_pre_specified(.task = bm.hc.bam.task)

el.hc.bam.task <- readr::read_rds(file = "data/rda/el.hc.bam.task.rds.gz")
el.pre.spe <- fn_predict_pre_specified(.task = el.hc.bam.task)

epi.hc.bam.task <- readr::read_rds(file = "data/rda/epi.hc.bam.task.rds.gz")
epi.pre.spe <- fn_predict_pre_specified(.task = epi.hc.bam.task)

bl.hc.bam.task <- readr::read_rds(file = "data/rda/bl.hc.bam.task.rds.gz")
bl.pre.spe <- fn_predict_pre_specified(.task = bl.hc.bam.task)

glh.hc.bam.task <- readr::read_rds(file = "data/rda/glh.hc.bam.task.rds.gz")
glh.pre.spe <- fn_predict_pre_specified(.task = glh.hc.bam.task)

dplyr::bind_rows(
  bm.pre.spe,
  el.pre.spe,
  epi.pre.spe,
  bl.pre.spe,
  glh.pre.spe
) %>%
  dplyr::filter(!cohort %in% c("DC", "TC")) %>%
  dplyr::group_by(hc_bam) %>%
  tidyr::nest() %>%
  dplyr::ungroup() ->
  pre_specified_specificity_nest

pre_specified_specificity_nest %>%
  dplyr::mutate(data = purrr::map(.x = data, .f = function(.d) {
    .d %>%
      dplyr::group_by(panel) %>%
      tidyr::nest() %>%
      dplyr::ungroup() ->
      .dd
    .dd %>%
      dplyr::mutate(data = purrr::map(.x = data, .f = function(.a) {
        .a %>%
          dplyr::group_by(cohort) %>%
          tidyr::nest() %>%
          dplyr::ungroup() ->
          .aa
        .aa %>%
          dplyr::mutate(data = purrr::map(.x = data, .f = function(.b) {
            .b %>%
              dplyr::slice(which.min(abs(.b$tnr - 0.9)))
          })) %>%
          tidyr::unnest(cols = data)
      })) %>%
      tidyr::unnest(cols = data)
  })) ->
  pre_specified_specificity_nest_filter

pre_specified_specificity_nest_filter %>%
  dplyr::filter(stringr::str_detect(string = hc_bam, pattern = "hc")) %>%
  tidyr::unnest(cols = data) %>%
  dplyr::select(hc_bam, panel, Cohort = cohort, AUC = auc_ci, Accuracy = acc_ci, Sensitivity = tpr_ci, Specificity = tnr_ci, PPV = ppv_ci, NPV = npv_ci, Kappa = kappa, F1 = f1) %>%
  dplyr::mutate(Specificity = gsub(pattern = "0.* \\(", replacement = "0.9 \\(", x = Specificity)) %>%
  dplyr::mutate(hc_bam = plyr::revalue(x = hc_bam, replace = c("bm.hc" = "BM", "el.hc" = "EL", "epi.hc" = "EPI", "bl.hc" = "BL", "glh.hc" = "Grade"))) %>%
  dplyr::mutate(panel = plyr::revalue(x = panel, replace = c("panel" = "TEPOC", "ca125" = "CA125", "panel_ca125" = "TEPOC + CA125"))) %>%
  dplyr::rename(Type = hc_bam, Model = panel) ->
  pre_specified_specificity_nest_filter_90

writexl::write_xlsx(x = pre_specified_specificity_nest_filter_90, path = "data/reviseoutput/sp90.xlsx")

# Save image --------------------------------------------------------------

save.image(file = "data/rda/10-pre-specified")
