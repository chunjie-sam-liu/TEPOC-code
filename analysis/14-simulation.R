# Metainfo ----------------------------------------------------------------

# @AUTHOR: Chun-Jie Liu
# @CONTACT: chunjie.sam.liu.at.gmail.com
# @DATE: Sat Sep 11 12:02:22 2021
# @DESCRIPTION: 14-simulation.R

# Library -----------------------------------------------------------------

library(magrittr)
library(ggplot2)
library(doParallel)


# Load data ---------------------------------------------------------------

bm.hc.performance <- readr::read_rds(file = "data/rda/bm.hc.performance.rds.gz")
bm.bam.performance <- readr::read_rds(file = "data/rda/bm.bam.performance.rds.gz")

# src ---------------------------------------------------------------------

source(file = "src/doparallel.R")
source(file = "src/utils.R")

# Function ----------------------------------------------------------------

fn_auc <- function(.d) {
  .new_data <- dplyr::sample_n(tbl = .d, size = nrow(.d), replace = TRUE)
  pROC::auc(.new_data$truth, .new_data$prob.M)
}
fn_bootstrape <- function(.d, .n = 5000) {
  .auc <- foreach(i = 1:.n, .combine = c, .packages = c("magrittr", "pROC")) %dopar% {
    set.seed(i)
    # .new_data <- dplyr::sample_n(tbl = .d, size = nrow(.d), replace = TRUE)
    .new_data <- .d %>%
      dplyr::mutate(sim_truth = sample(x = c("M", "B"), size = nrow(.d), replace = TRUE))
    pROC::auc(.new_data$sim_truth, .new_data$prob.M)
  }
  .auc
}
fn_plot_hist <- function(.d, .name, .type = "BM") {
  .vc <- fn_bootstrape(.d = .d$pred_data)
  .auc <- unique(.d$perf$auc)
  .p <-  1 - sum(.vc < .auc) / length(.vc)

  tibble::tibble(a = .vc) %>%
    ggplot() +
    geom_histogram(aes(x = a, y = stat(count) / sum(count))) +
    geom_vline(xintercept = 0.5, color = "blue") +
    geom_vline(xintercept = .auc, color = "red") +
    scale_x_continuous(expand = c(0,0), limits = c(0, 1)) +
    scale_y_continuous(expand = c(0,0)) +
    annotate(geom = "text", x = 0.85, y = 0.2, label = human_read_latex_pval(human_read(.auc), .s = "AUC="), size = 8) +
    annotate(geom = "text", x = 0.85, y = 0.18, label = human_read_latex_pval(human_read(.p), .s = "p="), size = 8) +
    labs(
      x = "",
      y = "Frequency",
      title = glue::glue("{.name} 5000 simulation")
    ) +
    theme(
      panel.grid = element_blank(),
      panel.background = element_rect(fill = NA, color = "black"),
      plot.title = element_text(hjust = 0.5, size = 20),
      axis.title.x = element_blank(),
      axis.title.y = element_text(size = 18)
    ) -> .plot;.plot

  ggsave(
    filename = glue::glue("{.name}-bootstrap-simulation-{.type}.pdf"),
    plot = .plot,
    device = "pdf",
    path = "data/reviseoutput",
    width = 6,
    height = 5
  )
  .plot
}

# Boot analysis -----------------------------------------------------------
fn_parallel_start(n_cores = 50)
# BM
# VC1
plot_VC1 <- fn_plot_hist(.d = bm.bam.performance$panel$performance$VC1, .name = "VC1")
# VC2
plot_VC2 <- fn_plot_hist(.d = bm.bam.performance$panel$performance$VC2, .name = "VC2")
# VC3
plot_VC3 <- fn_plot_hist(.d = bm.bam.performance$panel$performance$Tom, .name = "VC3")

# HC
# VC1
plot_VC1 <- fn_plot_hist(.d = bm.hc.performance$panel$performance$VC1, .name = "VC1", .type = "HC")
# VC2
plot_VC2 <- fn_plot_hist(.d = bm.hc.performance$panel$performance$VC2, .name = "VC2", .type = "HC")
# VC3
plot_VC3 <- fn_plot_hist(.d = bm.hc.performance$panel$performance$Tom, .name = "VC3", .type = "HC")

fn_parallel_stop()


# save image --------------------------------------------------------------

save.image(file = "data/rda/14-simulation.rda")
